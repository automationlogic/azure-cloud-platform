﻿#
# Script usage: ./cleanup.ps1 [-rgname <rgname>]


# Handle arguments first
param (
    [string]$rgname = "myResGroup"  # Default value
)
$keyVaultName = $rgname + "vault"
$fullSpName = "http://" + $keyVaultName + "-sp"
$fullRoleName = $rgname + "RoleSP"
$adAppName = $keyVaultName + "-sp"

echo "-------remove role assignments"
$roleAssignment=Get-AzureRmRoleAssignment -ServicePrincipalName $fullSpName
Remove-AzureRmRoleAssignment -ObjectId $roleAssignment.ObjectId.toString() -RoleDefinitionName $fullRoleName

echo "-------remove role definitions"
$roleDefinition=Get-AzureRmRoleDefinition -Name $fullRoleName
Remove-AzureRmRoleDefinition -Id $roleDefinition.Id.toString() -Force

echo "-------remove service principles"
$servicePrincipal=Get-AzureRmADServicePrincipal -ServicePrincipalName $fullSpName
echo $servicePrincipal
Remove-AzureRmADServicePrincipal -ObjectId $servicePrincipal.Id.toString() -Force

echo "-------remove application"
$adApplication=Get-AzureRmADApplication -DisplayNameStartWith $adAppName
Remove-AzureRmADApplication -ObjectId $adApplication.ObjectId.toString() -force

echo "-------remove resource group"
Remove-AzureRmResourceGroup -name $rgname -Force
