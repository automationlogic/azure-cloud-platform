#!/bin/bash
set -ex


######################
#   Initial setup   #
####################

# Initialise variables
stack_name=$1
region=$2
VSTSAccountName=$3

if [[ "$#" -ne 3 ]]; then
  echo "Usage: bash create.sh <stack name> <region> <VSTS account name>"
  exit 1
fi

# Generate random passwords, 32 characters long, allow A-Za-z0-9+=/
sshPassword=$(LC_ALL=C tr -dc 'A-Za-z0-9+=/' </dev/urandom | fold -w 32 | head -n 1)
mysqlPassword=$(LC_ALL=C tr -dc 'A-Za-z0-9+=/' </dev/urandom | fold -w 32 | head -n 1)

# Check if passwords were actually generated, if not fall back to openssl
if [ -z "$sshPassword" ] || [ -z "$mysqlPassword" ]; then
  sshPassword=$(openssl rand -base64 32)
  mysqlPassword=$(openssl rand -base64 32)
fi

# set usernames
sshUser=$(jq -r '.parameters.sshUser.value' < ARM/parameters.json.template)
mysqlUser=$(jq -r '.parameters.mysqlUser.value' < ARM/parameters.json.template)

# create main resource group
echo "Creating resource group for main resources..."
az group create --name "${stack_name}"  -l "${region}" >/dev/null




#######################
#    Main storage    #
#####################

# This creates a storage account inside the resource group
# for config scripts, certificates, etc.

echo "Creating storage inside main resource group..."
main_storage="${stack_name}storage"
main_storage_lc=$(echo "${stack_name}" | tr '[:upper:]' '[:lower:]')

# shellcheck disable=SC2016
AZURE_STORAGE_ACCOUNT=$(az storage account list | jq -r --arg SGN "${main_storage_lc}" '.[] | select (.resourceGroup == $SGN) | .name')

if [[ -z ${AZURE_STORAGE_ACCOUNT} ]]; then
	AZURE_STORAGE_ACCOUNT="${main_storage}"
	az group deployment create --resource-group "${stack_name}" --template-file ARM/storage.json --parameters storageAccountName="${main_storage}" >/dev/null
else
	echo "Storage account ${main_storage} already exists"
fi

AZURE_STORAGE_ACCESS_KEY=$(az storage account keys list -g "${stack_name}" -n "${AZURE_STORAGE_ACCOUNT}" --output json | jq -r '.[0] | .value')
export AZURE_STORAGE_ACCOUNT
export AZURE_STORAGE_ACCESS_KEY
main_storage_key="${AZURE_STORAGE_ACCESS_KEY}"

echo "Creating main storage container..."
az storage container create --name "${stack_name}-container" >/dev/null




######################
#     Key Vault     #
####################

# create KeyVault
echo "Creating KeyVault..."
vault_name="${stack_name}"vault
az keyvault create -n "${vault_name}" -g "${stack_name}" -l "${region}" --enabled-for-deployment true --enabled-for-template-deployment true >/dev/null

# create SP and cert
# then upload cert to KeyVault
echo "Creating service principal and certificate..."
az ad sp create-for-rbac -n "${vault_name}-sp" --keyvault "${vault_name}" --cert "${vault_name}"-cert --create-cert >/dev/null


# give permissions for SP to access vault
sub_id=$(az account show | jq -r '.id')
kv_role_json=$(sed -e s/SUBIDHERE/"${sub_id}"/g -e s/STACKNAME/"${stack_name}"/g < ARM/kv_role.json )

az role definition create --role-definition "${kv_role_json}" >/dev/null
az role assignment create --assignee http://"${vault_name}-sp"  --role "${stack_name}RoleSP" >/dev/null

secret_permissions=(backup delete get list purge recover restore set)
key_permissions=(decrypt get import list recover sign)

# shellcheck disable=SC2086
az keyvault set-policy -n "${vault_name}" --spn http://"${vault_name}-sp" --secret-permissions ${secret_permissions[*]} --key-permissions ${key_permissions[*]} >/dev/null

sourceVaultID=$(az keyvault show -n "${vault_name}" | jq -r '.id')
certificateUrl=$(az keyvault secret list-versions --vault-name "${vault_name}" -n "${vault_name}"-cert | jq -r '.[] | select(.attributes.enabled==true) | .id')




###################################
#     Upload files & secrets     #
#################################

echo "Compressing app files..."
tar -czvf blobs/app-archive app/ >/dev/null


TENANT_ID=$(az account show --output json | jq -r '.tenantId')

# add SP login command to templates
echo "Creating templates..."
(
	cd templates || exit
	for temp_file in *; do
		new_file="${temp_file//.template/}"
		sed -e "s/STACKNAME/${stack_name}/g" -e "s/TENANT/${TENANT_ID}/g" <  "${temp_file}"  >../blobs/"${new_file}"
		echo "${new_file} created"
	done
)


echo "Generating key pair..."
yes | ssh-keygen -f blobs/app-key -N '' >/dev/null

# Upload files to blob storage
echo "Uploading files..."
(
	cd blobs || exit
	for blob_file in *; do
		az storage blob upload --container-name "${stack_name}-container" --file "${blob_file}" --name "${blob_file}" >/dev/null && \
		echo "${blob_file} uploaded" \
    	|| echo "${blob_file} failed to upload"
	done
)

rm blobs/app-key*

# Add secrets to keyvault
echo "Adding secrets to KeyVault..."
az keyvault secret set --vault-name "${vault_name}" --name 'storageAccount' --value "${AZURE_STORAGE_ACCOUNT}" >/dev/null
az keyvault secret set --vault-name "${vault_name}" --name 'storageAccessKey' --value "${AZURE_STORAGE_ACCESS_KEY}" >/dev/null
az keyvault secret set --vault-name "${vault_name}" --name 'sshUser' --value "${sshUser}" >/dev/null
az keyvault secret set --vault-name "${vault_name}" --name 'sshPassword' --value "${sshPassword}" >/dev/null
az keyvault secret set --vault-name "${vault_name}" --name 'mysqlUser' --value "${mysqlUser}" >/dev/null
az keyvault secret set --vault-name "${vault_name}" --name 'mysqlPassword' --value "${mysqlPassword}" >/dev/null




########################
#    Logging setup    #
######################

# This will create a separate resource group for
# log storage that all resource groups will write to.

echo "Creating resource group for log storage..."
log_group_name="logstorage"
log_group_name_lc=$(echo ${log_group_name} | tr '[:upper:]' '[:lower:]')
# shellcheck disable=SC2016
if [[ -z $(az group list | jq -r --arg SGN "${log_group_name}" '.[] | select(.name == $SGN)') ]]; then
	az group create --name "${log_group_name}"  -l "${region}" >/dev/null
else
	echo "Resource group for log storage already exists"
fi

echo "Creating log storage account..."
# shellcheck disable=SC2016
log_storage_name="${log_group_name}"$(date +%s)
# shellcheck disable=SC2016
AZURE_STORAGE_ACCOUNT=$(az storage account list | jq -r --arg SGN "${log_group_name_lc}" '.[] | select (.resourceGroup == $SGN) | .name')

if [[ -z ${AZURE_STORAGE_ACCOUNT} ]]; then
	AZURE_STORAGE_ACCOUNT=${log_storage_name}
	az group deployment create --resource-group "${log_group_name}" --template-file ARM/storage.json --parameters storageAccountName="$AZURE_STORAGE_ACCOUNT" >/dev/null
else
	echo "Storage account ${log_storage_name} already exists"
fi

AZURE_STORAGE_ACCESS_KEY=$(az storage account keys list -g "${log_group_name}" -n "${AZURE_STORAGE_ACCOUNT}" --output json | jq -r '.[0] | .value')
export AZURE_STORAGE_ACCOUNT
export AZURE_STORAGE_ACCESS_KEY

# create logs for API calls and store them in storage account
# shellcheck disable=SC2016
storage_account_id=$(az storage account list | jq -r --arg SGN "${log_group_name_lc}" '.[] | select(.resourceGroup | contains($SGN)) | .id')

echo "Creating log profile..."
# Temporarily commented out - Getting error --> Parameter 'LogProfileResource.retention_policy' can not be None (??!!)
#
#if [[ $(az monitor log-profiles list | jq '. | length') == 0 ]]
#then
#	az monitor log-profiles create -n "log-profile" --enabled ENABLED --locations "${region}" --days 365 -l "${region}" \
#		--categories Write Delete Action --storage-account-id "${storage_account_id}"
#else
#	echo "Log profile already exists"
#fi




#################################
#  Create main infrastructure  #
###############################

# Create Team Services account and project
# Try to create first; if it fails then link to existing account
az group deployment create --resource-group "${stack_name}" --template-file ARM/vsts.json \
	--parameters accountName="${VSTSAccountName}" \
		projectName="${stack_name}-project" 2> /dev/null ||
az group deployment create --resource-group "${stack_name}" --template-file ARM/vsts.json \
	--parameters accountName="${VSTSAccountName}" \
    projectName="${stack_name}-project" \
		operationType=Link

project_link="https://${VSTSAccountName}.visualstudio.com/_details/security/tokens/Edit"

# check if PAT token environment variable is set for authentication
while [[ -z "${VSTS_CLI_PAT}" ]]; do
  echo "PAT token not provided."
  echo "If you do not have a PAT token, go to: ${project_link}"
  echo -n "PAT token: "
  read -s VSTS_CLI_PAT
done


# Create the rest of the infrastructure
echo "Creating parameters file..."
sed -e "s@VAULTID@${sourceVaultID}@g" < ARM/parameters.json.template > ARM/parameters.json

echo "Creating resources from main template..."
az group deployment create --resource-group "${stack_name}" --template-file ARM/template.json \
	--parameters ARM/parameters.json \
		stackName="$stack_name" \
		storageAccountName="${main_storage}" \
		storageAccountKey="${main_storage_key}" \
		sourceVaultID="${sourceVaultID}"  \
		certificateUrl="${certificateUrl}" \
		storageAccountID="${storage_account_id}" \
		VSTSAccountName="${VSTSAccountName}" \
		projectName="${stack_name}-project" \
		--output json | jq '{id: .id, outputs: .properties.outputs}'


# echo "Configuring Azure Database for MySQL Server logging..."
# SERVER_NAME=$(az mysql server list --output json| jq -r '.[].name')
# az mysql server configuration set --name slow_query_log --resource-group "${stack_name}" --server "${SERVER_NAME}" --value ON >/dev/null
# az mysql server configuration set --name long_query_time --resource-group "${stack_name}" --server "${SERVER_NAME}" --value 10 >/dev/null
# az mysql server configuration set --name log_throttle_queries_not_using_indexes --resource-group "${stack_name}" --server "${SERVER_NAME}" --value 0 >/dev/null
# az mysql server configuration set --name log_slow_admin_statements --resource-group "${stack_name}" --server "${SERVER_NAME}" --value ON >/dev/null
# az mysql server configuration set --name log_slow_slave_statements --resource-group "${stack_name}" --server "${SERVER_NAME}" --value ON >/dev/null
# az mysql server configuration set --name log_slow_admin_statements --resource-group "${stack_name}" --server "${SERVER_NAME}" --value ON >/dev/null
# az mysql server configuration set --name log_queries_not_using_indexes --resource-group "${stack_name}" --server "${SERVER_NAME}" --value ON >/dev/null
# az mysql server configuration set --name log_bin_trust_function_creators --resource-group "${stack_name}" --server "${SERVER_NAME}" --value ON >/dev/null



########################
#       Cleanup       #
######################

echo "Cleaning up..."
rm blobs/app-archive

# template cleanup
rm blobs/app_install.sh
rm blobs/blob_auth
rm blobs/db_init.sh
