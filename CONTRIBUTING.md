# Contributing

This repo has an **.editorconfig** file so you should install EditorConfig in your code editor/IDE to maintain code style consistency.

* VS Code - https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig
* Atom - https://atom.io/packages/editorconfig

To make a contribution to this repo, first clone it and then raise a PR. This should be a flat
series of commits against the most recent `master`, with work squished into logical chunks (not
necessarily all one commit, though this could be the case sometimes).

Commit messages should be ideally follow the recommendations at https://github.com/alphagov/styleguides/blob/master/git.md
