﻿#
# Script usage: ./create.ps1 [-rgname <rgname> -region <region> -mail <email>]


# Handle arguments first
param (
    [string]$rgname = "myResGroup",  # Default value
    [string]$region = "eastus",      # Default value
    [string]$mail = "none"           # Default value
)
# set usernames, get password values from user
$jenkins_pw  = Read-host -Prompt 'Enter Jenkins password: ' -AsSecureString
$ssh_pw      = Read-host -Prompt 'Enter SSH password: '     -AsSecureString
$sql_pw      = Read-host -Prompt 'Enter SQL password: '     -AsSecureString

$paramRaw      = Get-Content .\ARM\parameters.json.template -Raw
$paramObject   = ConvertFrom-Json -InputObject $parmRaw
$jenkinsUser   = $paramObject.parameters.jenkinsUser.value
$sshUser       = $paramObject.parameters.sshUser.value
$mysqlUser     = $paramObject.parameters.mysqlUser.value

echo "-------create the resource group"
New-AzureRmResourceGroup -Name $rgname -Location $region

echo "-------create the storage account"
New-AzureRmResourceGroupDeployment -Name storage -ResourceGroupName $rgname `
  -TemplateFile ARM/storage.json -storageAccountType Standard_LRS -stackName $rgname


echo "-------creating KeyVault"
$keyVaultName = $rgname + "vault"
$user      = Get-AzureRmADUser -UserPrincipalName $mail

New-AzureRmResourceGroupDeployment -Name $keyVaultName -ResourceGroupName $rgname  `
  -TemplateFile ARM/keyvault.json -keyVaultName $keyVaultName -objectId $user.Id.ToString()

echo "-------creating service principal with a Keyvault certificate"
$validityInMonths=12
$principalName="$keyVaultName-sp"

function New-KeyVaultSelfSignedCert {
  param($vaultName, $certificateName, $subjectName, $validityInMonths, $renewDaysBefore)

  $policy = New-AzureKeyVaultCertificatePolicy `
              -SubjectName $subjectName `
              -ReuseKeyOnRenewal `
              -IssuerName 'Self' `
              -ValidityInMonths $validityInMonths `
              -RenewAtNumberOfDaysBeforeExpiry $renewDaysBefore

  $op = Add-AzureKeyVaultCertificate `
              -VaultName $vaultName `
              -CertificatePolicy $policy `
              -Name $certificateName

  while ( $op.Status -ne 'completed' ) {
    Start-Sleep -Seconds 1
    $op = Get-AzureKeyVaultCertificateOperation -VaultName $vaultName -Name $certificateName
  }
  (Get-AzureKeyVaultCertificate -VaultName $vaultName -Name $certificateName).Certificate
}
$certName = "$keyVaultName-cert"
$cert = New-KeyVaultSelfSignedCert -vaultName $keyVaultName `
                                   -certificateName $certName `
                                   -subjectName "CN=$principalName" `
                                   -validityInMonths $validityInMonths `
                                   -renewDaysBefore 1

Write-Verbose "Certificate generated $($cert.Thumbprint)"

$certString = [Convert]::ToBase64String($cert.GetRawCertData())

New-AzureRmADServicePrincipal -DisplayName $principalName `
                              -CertValue $certString `
                              -EndDate $cert.NotAfter.AddDays(-1)

echo "-------creating a zip of app folder"
$source = $pwd.ToString() + "\app"
$destination = $pwd.ToString() + "\blobs\app-archive.zip"

If(Test-path $destination) {Remove-item $destination}
Add-Type -assembly  "system.io.compression.filesystem"
[io.compression.zipfile]::CreateFromDirectory($source, $destination)

echo "-------give permissions for SP to access vault"
$subscription = Get-AzureRmSubscription
$sub_id = $subscription.Id.ToString()                                 #store Azure subscription id
$tenant_id = $subscription.TenantId.ToString()                        #store Azure subscription tenant id
$kv_role_json_path = $pwd.ToString()+"\ARM\kv_role.json"              #get relative path to json file
$kv_role_json = (Get-Content $kv_role_json_path) | ForEach-Object {$_ -replace 'SUBIDHERE', $sub_id -replace 'STACKNAME', $rgname} #populate var with replaced json file
echo $kv_role_json > "$pwd\temp.txt" # output amended json file to temporary file

Start-Sleep -s 20 #this will ensure that the service principal has time to finish

New-AzureRmRoleDefinition -InputFile "$pwd\temp.txt" # create new role definition
If(Test-path "$pwd\temp.txt") {Remove-item "$pwd\temp.txt"} #remove temporary file

$fullSpName = "http://" + $keyVaultName + "-sp"
$fullRoleName = $rgname + "RoleSP"

New-AzureRmRoleAssignment -ServicePrincipalName $fullSpName -RoleDefinitionName $fullRoleName #assign role to service prinicipal

Set-AzureRmKeyVaultAccessPolicy -VaultName $keyVaultName -ServicePrincipalName $fullSpName -PermissionsToSecrets get,list,set,delete,backup,restore,recover,purge -PermissionsToKeys decrypt,get,import,list,recover,sign

$keyVaultRef = Get-AzureRmKeyVault -VaultName $keyVaultName
$sourceVaultID=$keyVaultRef.ResourceId

$keyVaultCertRef = Get-AzureKeyVaultSecret -VaultName $keyVaultName -Name $keyVaultName-cert
$certificateUrl = $keyVaultCertRef.Id

echo "-------Blob storage configuration"
$azure_storage_account=$rgname+"storage"
$storageKeyRaw=get-azurermstorageaccountkey -ResourceGroupName $rgname -Name $azure_storage_account
$azure_storage_access_key=$storageKeyRaw.Value.GetValue(0).toString()
$storageContainer=$rgname+"-container"

echo "-------creating the storage container"
$storageContext = New-AzureStorageContext -StorageAccountName $azure_storage_account -StorageAccountKey $azure_storage_access_key
New-AzureStorageContainer -name $storageContainer -Context $storageContext

echo "-------Creating templates"
$files = @(Get-ChildItem "$pwd\templates")
foreach ($file in $files) {
    $new_file=echo $file.name | % { $_ -replace ".template”, “” }
    (Get-Content $file.Fullname) | ForEach-Object {$_ -replace 'STACKNAME', $rgname -replace 'TENANT', $tenant_id} | Out-File "$pwd\blobs\$new_file"
    echo "$new_file created"
}
echo "-------Creating Ansible inventory scripts"
cat "$pwd\blobs\inventory.sh" | % { $_ -replace '#APP_PASS#',$ssh_pw -replace '#APP_USER#', $sshUser } > "$pwd\ansible\inventory.sh"
#######password replacement above is not finished
#######old file to be removed 'blobs/inventory.sh'
#######file 'ansible\inventory.sh' requires to change file permission, requires execute permission

#######create ansible tarball here ### maybe use zip

#######key-gen required here - output into blobs\app-key

echo "-------uploading files..."
$bFiles = @(Get-ChildItem "$pwd\blobs")
foreach ($bFile in $bFiles) {
    Set-AzureStorageBlobContent -Container $storageContainer -file $bFile.FullName -Context $storageContext
    echo "$bFile uploaded"
}

echo "--------creating parameters file"
cat .\ARM\parameters.json.template | % { $_ -replace “VAULTID”, $sourceVaultID } | Out-File .\ARM\parameters.json

