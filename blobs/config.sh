#!/bin/bash
set -ex


while ( ! (find /var/log/azure/Microsoft.EnterpriseCloud.Monitoring.OmsAgentForLinux/*/extension.log -print0 | xargs -0 grep "Enable succeeded")); do
	echo "Waiting for OMS Agent to install..."
	sleep 5
done

# create cert file
(
	cd /var/lib/waagent
	awk '/BEGIN PRIVATE/ {f=1}; f && c==1; /END PRIVATE/ {f=0; c++} ' Certificates.pem >>newCert.pem
	awk '/BEGIN CERT/ {f=1}; f && c==1; /END CERT/ {f=0; c++} ' Certificates.pem >>newCert.pem
)

echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ wheezy main" | sudo tee /etc/apt/sources.list.d/azure-cli.list
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -

apt-get update && apt-get -y install nodejs apache2 jq mysql-client apt-transport-https azure-cli --allow-unauthenticated

cat > export_db_credentials.sh <<_END_
#!/usr/bin/env bash

export DB_CONNECTIONSTRING="${SQL_SERVER_NAME}.mysql.database.azure.com"
export DB_USERNAME="${SQL_ADMIN_USER}@${SQL_SERVER_NAME}"
export DB_PASSWORD="${SQL_ADMIN_PASSWORD}"
export DB_NAME=todo

\$@
_END_

chmod +x export_db_credentials.sh

chmod +x sql_schema.sh

# change jenkins to waagent in blob_auth

sed 's/lib\/jenkins/lib\/waagent/g' < blob_auth > blob_waagent

. blob_waagent ./db_init.sh  >/dev/null 2>&1

export STACK_NAME
chmod +x app_install.sh

. blob_waagent ./app_install.sh

# shellcheck disable=SC2024
sudo tee -a "/home/${SSH_USER}/.ssh/authorized_keys" < app-key.pub
chmod 600 "/home/${SSH_USER}/.ssh/authorized_keys"
