#!/usr/bin/env bash

set -xe

echo "Configure MySQL schema"
# If MySQL client is not installed, fail!
mysql -h "${DB_CONNECTIONSTRING}" -p"${DB_PASSWORD}" -u "${DB_USERNAME}" < schema.sql
