
# Azure Cloud Platform

This product automates the task of spinning up infrastructure and deploying custom applications on top on the Azure public cloud. **Please note that the product is currently under development and might have still some issues**.

The objective of this project is to provide a one-click solution for organizations and developers who need to focus on the application and worry less about creating and managing the infrastructure of the platform, while simultaneously providing flexibility and customizability for more experienced DevOps users. A similar (and more advanced) solution has been developed using AWS and can be found [here](https://bitbucket.org/automationlogic/aws-cloud-platform).

This repository currently contains one simple CRUD application written in Node.js and using a MySQL database. In the following weeks we should be able to add more examples using different frameworks/databases.

Each application is deployed on top of an Azure infrastructure stack including a Scale Set of VMs or containers, a load balancer, security rules, logging, metrics, monitoring, a database for the application, a key vault to store secrets and credentials and a continuous integration/continuous deployment (CI/CD) pipeline using Visual Studio Team Services (VSTS). Once the application code is pushed to the repository, the pipeline should build and deploy the web application automatically.

We are also exploring the possibility of deploying the applications on top Azure Application Services. This would remove the need to create all the underlying infrastructure, such as the ScaleSets, Load Balancer, etc. and would potentially make the integration with VSTS much simpler. Please note that the current instructions below only cover the deployment using infrastructure.

The deployment is started using a script (as described below) but we're currently investigating the option of starting the deployment directly from the VSTS web console. VSTS provides all the required elements to create and run a CI/CD pipeline, but also includes the possibility to create all the underlying infrastructure in Azure as part of the build. Please note that the current instructions below only cover the deployment using infrastructure.

This project is released under the [Apache License](https://www.apache.org/licenses/LICENSE-2.0).

## Requirements

* You'll need `ssh-keygen` installed locally, if using `bash`. This typically comes by default on Mac OS X / Linux but might need installing separately if you're on a lightweight OS distribution.
* `jq` installed locally. The binaries can be found [here](https://stedolan.github.io/jq/download)
* `git` installed locally. The binaries can be found [here](https://git-scm.com/downloads).
* An Azure account (this can be corporate or personal, paid or trial).

## Getting started

The deployment can be started from a local shell or from the Azure Cloud shell (top right of the Azure web console). The Azure Cloud shell supports both `bash` and `PowerShell`.

![Azure Cloud Shell](/docs/img/cloudshell.png)

*Accesing the Azure Cloud shell*

Alternatively, open a terminal with a `bash` shell in Linux/OS X or a `PowerShell` terminal under Windows.

Follow the instructions below:

1. Clone this repo, e.g.  

	`git clone https://bitbucket.org/automationlogic/azure-cloud-platform`

2. If you're not using the Azure Cloud Shell, install the Azure command line tools with the instructions at https://docs.microsoft.com/en-us/cli/azure/install-azure-cli. Ensure you can run the `az` command, and then log in with `az login` using your previously-created credentials.

3. Modify `ARM/parameters.json.template` with your app repository URL and the branch you wish to build from. You can optionally change the usernames for SSH and database login, though defaults are set. The SSH and database passwords will be automatically generated during runtime and will be stored in the Key Vault.

4. (Optional) Set the VSTS_CLI_PAT environment variable using your VSTS PAT token as below:

	**bash**: `export VSTS_CLI_PAT=hftq6ta674o6o2lpbt5hih7ze3jwnxxxxxxxxxxxxxxxxxxxxxx`  

	**PowerShell**:	`$env:VSTS_CLI_PAT = "hftq6ta674o6o2lpbt5hih7ze3jwnxxxxxxxxxxxxxxxxxxxxxx"`

	If you do not set this variable, you will be prompted during the execution of `create.sh` to input a token (you can follow the link provided during this stage to create one).  

	For more information on PAT tokens: https://docs.microsoft.com/en-us/vsts/accounts/use-personal-access-tokens-to-authenticate

## Common operations

### Creating the infrastructure

Perform the initial creation with the following command:

`./create.sh <resource_group> <region> <VSTS_account_name>`

* `resource_group` is the name of the Azure resource group that will contain most of your services.
* `region` is the default Azure region where the services will be deployed. At the time of writing, only certain regions are supported. Whilst we have not created an exhaustive list, the following are known to work: `eastus` and `westeurope`. VSTS resources will be deployed in `centralus` by default.
* `VSTS_account_name` is the name of your existing VSTS account. For instance, if the URL of your account is `https://automationlogic.visualstudio.com/` then the `VSTS_account_name` should be simply `automationlogic`. If the account doesn't exist, then it will be automatically created.

When the deployment is complete, your application will be available at `http://web-<stackname>.<region>.cloudapp.azure.com/`

For convenience, this value will appear in the automatically generated output for the stack.

### Deleting the infrastructure

Note that this will delete all resources inside a resource group, including the ones which might have been created manually afterwards.

`./cleanup.sh <stackname> [--delete-logs]`

The `--delete-logs` flag is optional, depending on whether you wish to retain logs or remove them. You can also provide this option without providing a stack name.

### Updating the app

You can perform application updates by doing a git push to location as configured by the values for `branch` and `repo` in `ARM/parameters.json`.
