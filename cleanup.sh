#!/bin/bash

stack_name=$1

if [[ "$#" == 0 ]]; then
  echo "Usage:  bash cleanup.sh <stack name>"
  exit 1
fi


# optional flag to delete all logs
while (( "$#" )); do
	case "$1" in
		--delete-logs)
			echo "Deleting log profile..."
			az monitor log-profiles delete -n "log-profile"
			echo "Deleting storage..."
			# shellcheck disable=SC2016
			log_storage=$(az storage account list | jq -r --arg SGN "logstorage" '.[] | select (.resourceGroup == $SGN) | .name')
			az storage account delete --resource-group "logstorage" --name "${log_storage}"
			echo "Deleting log resource group..."
			az group delete --name "logstorage" --yes
			if [[ "${stack_name}" == --delete-logs ]]; then  # if no stack name provided
				exit 0
			fi
			shift
			;;
		*)
		shift
		;;
	esac
done


echo "Deleting role assignment..."
assignment_ids=$(az role assignment list | jq -r ".[] | select(.properties.principalName | contains(\"http://${stack_name}vault-sp\")) | .id")
# shellcheck disable=SC2086
az role assignment delete --ids ${assignment_ids}

echo "Deleting role definition..."
az role definition delete -n "${stack_name}"RoleSP


echo "Deleting service principals..."
object_id=$(az ad sp list | jq -r ".[] | select(.displayName | contains(\"${stack_name}vault-sp\")) | .objectId")
az ad sp delete --id "${object_id}"


echo "Deleting resource group..."
az group delete --name "${stack_name}" --yes


echo "Done."
